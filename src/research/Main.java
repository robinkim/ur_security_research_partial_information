package research;

import edu.berkeley.nlp.lm.StupidBackoffLm;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rk7aq
 */
public class Main {

    static StupidBackoffLm google;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
//Example of language model that takes binary
        //NgramLanguageModel textModel = LmReaders.readLmBinary("/Users/rk7aq/Desktop/lmText.binary"); //brown corpus text model
        //NgramLanguageModel posModel = LmReaders.readLmBinary("/Users/rk7aq/Desktop/lmPOS.binary"); //brown corpos POS model

//test 5 words subsets        
        Sentence test = new Sentence("Government managers at all levels often assume their own cost of capital to be much lower than that of the private sector , effectively lowering a project ’ s expected returns");
        for (int i = 0; i < test.lengthFiveSubSentence().size(); i++) {
            System.out.println(test.lengthFiveSubSentence().get(i));
        }

//Commence GOOGLE! 
        long startLoadTime = System.nanoTime();
        System.out.println("Commence GOOGLE!");
        System.out.println("It will take a while... approximately 2-3mins");
        google = commenceGoogle();
        System.out.println("Google has arrived..");
        System.out.println("");

//Load Vocab Module
        System.out.println("Start loading Vocabs...\n");
        GoogleVocab.fill();
        System.out.println("Vocab Loaded.");

//Loading BackEnd Data Time
        long LoadDuration = System.nanoTime() - startLoadTime;
        System.out.println("Elapsed Loading Time: " + TimeUnit.SECONDS.convert(LoadDuration, TimeUnit.NANOSECONDS) + " sec\n");

//Start Timer
        long startCalculate = System.nanoTime();
        System.out.println("Calculating Ngram...");

//ArrayList of Sentences to be analyzed -----> Convert this to scanner later on
        /*
         ArrayList<Sentence> testArray = new ArrayList();
         Scanner scan = new Scanner(new File("/Users/rk7aq/Desktop/cleanedText"));
         while(scan.hasNext()){
         testArray.add(new Sentence(scan.nextLine()));
         }*/
        ArrayList<Sentence> testArray = new ArrayList();
        testArray.add(new Sentence("steady on the stool ???le ??"));
        testArray.add(new Sentence("Summer? s??r?ed ?p ??? pap?r? i??ide ??"));

//Call Methods
        manualSentenceWithRedactionProb(testArray, 1, 2);
        //sentenceWithLettersMissing();

        /*        
         //Target String goes here (Make scanner to get textfile for ArrayList<Sentence> later)
         System.out.println("Sentence:");
         Sentence problem = new Sentence("Yet private investors and companies too frequently fail to fill the gap even when their coffers are full");
         Sentence original = problem.copyOf();

         // Remove desired letters OR
       
         problem.removeLetter('t');
         problem.removeLetter('n');
         problem.removeLetter('a');
         problem.removeLetter('s');
         problem.removeLetter('l');
         problem.removeLetter('e');
         problem.removeLetter('o');
         
         // Remove random letters
         double prob = 0.5; // around (prob) completion range
         problem.randomLetterRedaction(prob);

         // Sentence profile
         System.out.println("Sentence Profile- ");
         problem.print();

         // Sequencial Calculation of Log Probs        
        
         //calculateProb(sen);

         // take user input to narrow the search
         int counter = 3; //initial n-gram

         Sentence probable = problem.subSentence(counter - 1);
         StringBuffer probableAppend = new StringBuffer();
         Sentence chosen = new Sentence(" ");

         while (probable.wordCount() < problem.wordCount() + 1) {
         counter++;
         ArrayList<Sentence> possibleSentences = calculateProb(probable);
         System.out.println("Original Sentence: " + problem.toString());
         System.out.println("Probable Sentence: " + probable.toString());

         //  int selected  = 0; //always the first choice  --------------> INPUT SELECTION ALGORITHM public int selectionAlgo();
         Scanner reader = new Scanner(System.in);
         System.out.println("");
         System.out.println("Select a sentence");
         int selected = reader.nextInt() - 1;
         System.out.println("");

         chosen = possibleSentences.get(selected);
         probableAppend = new StringBuffer(chosen.toString());
         probableAppend.append(" " + problem.wordAt(counter - 1).toString());
         probable = new Sentence(probableAppend.toString());
         }
         */
        // ArrayList<Sentence> result = calculateProb(probable);
        long CalculatingTime = System.nanoTime() - startCalculate;

        System.out.println("");
        System.out.println("Elapsed Loading Time: " + TimeUnit.SECONDS.convert(CalculatingTime, TimeUnit.NANOSECONDS) + " sec");

    }

//********************************************* METHODS ******************************************
    public static void manualSentenceWithLettersMissing(ArrayList<Sentence> sentences, ArrayList<Character> missingLetters, int ngramCounter) throws IOException, ClassNotFoundException {
        for (int i = 0; i < sentences.size(); i++) {
//Target String goes here 
            System.out.println("Sentence:");
            Sentence problem = sentences.get(i);
            Sentence original = problem.copyOf();

//remove letters            
            for (int k = 0; k < missingLetters.size(); k++) {
                problem.removeLetter(missingLetters.get(k));
            }

// Sentence profile
            System.out.println("Sentence Profile- ");
            problem.print();

// take user input to narrow the search
            Sentence probable = problem.subSentence(ngramCounter - 1);
            StringBuffer probableAppend = new StringBuffer();
            Sentence chosen = new Sentence(" ");

            while (probable.wordCount() < problem.wordCount() + 1) {
                ngramCounter++;
                ArrayList<Sentence> possibleSentences = calculateProb(probable);
                System.out.println("Original Sentence: " + problem.toString());
                System.out.println("Probable Sentence: " + probable.toString());

                //  int selected  = 0; //always the first choice  --------------> INPUT SELECTION ALGORITHM public int selectionAlgo();
                Scanner reader = new Scanner(System.in);
                System.out.println("");
                System.out.println("Select a sentence");
                int selected = reader.nextInt() - 1;
                System.out.println("");

                chosen = possibleSentences.get(selected);
                probableAppend = new StringBuffer(chosen.toString());
                probableAppend.append(" " + problem.wordAt(ngramCounter - 1).toString());
                probable = new Sentence(probableAppend.toString());
            }
// show comparison result
            System.out.println("");
            problem.printComparison(original, probable);
            System.out.println("");
        }
    }

    public static void manualSentenceWithRedactionProb(ArrayList<Sentence> sentences, double redactionProb, int ngramCounter) throws IOException, ClassNotFoundException {
        Integer initialNgram = new Integer(ngramCounter);
        for (int i = 0; i < sentences.size(); i++) {
//Target String goes here 
            System.out.println("Sentence:");
            Sentence problem = sentences.get(i);
            Sentence original = problem.copyOf();

// Remove random letters
            problem.randomLetterRedaction(redactionProb);

// Sentence profile
            System.out.println("Sentence Profile- ");
            problem.print();

// take user input to narrow the search
            ngramCounter = initialNgram;
            Sentence probable = problem.subSentence(initialNgram - 1);
            StringBuffer probableAppend = new StringBuffer();
            Sentence chosen = new Sentence(" ");

            while (probable.wordCount() < problem.wordCount() + 1) {
                ngramCounter++;
                ArrayList<Sentence> possibleSentences = calculateProb(probable);
                System.out.println("Original Sentence: " + problem.toString());
                System.out.println("Probable Sentence: " + probable.toString());

                //int selected = 0; //always the first choice  --------------> INPUT SELECTION ALGORITHM public int selectionAlgo();
                Scanner reader = new Scanner(System.in);
                System.out.println("");
                System.out.println("Select a sentence");
                int selected = reader.nextInt() - 1;
                System.out.println("");

                chosen = possibleSentences.get(selected);
                probableAppend = new StringBuffer(chosen.toString());
                probableAppend.append(" " + problem.wordAt(ngramCounter - 1).toString());
                probable = new Sentence(probableAppend.toString());
            }
// show comparison result
            System.out.println("");
            problem.printComparison(original, probable);
            System.out.println("");
        }
    }

    public static void autoSentenceWithRedactionProb(ArrayList<Sentence> sentences, double redactionProb, int ngramCounter) throws IOException, ClassNotFoundException {
        Integer initialNgram = new Integer(ngramCounter);
        for (int i = 0; i < sentences.size(); i++) {
//Target String goes here (Make scanner to get textfile for ArrayList<Sentence> later)
            System.out.println("Sentence:");
            Sentence problem = sentences.get(i);
            Sentence original = problem.copyOf();

// Remove random letters
            problem.randomLetterRedaction(redactionProb);
// Sentence profile
            System.out.println("Sentence Profile- ");
            problem.print();
// take user input to narrow the search
            ngramCounter = initialNgram;
            Sentence probable = problem.subSentence(ngramCounter - 1);
            StringBuffer probableAppend = new StringBuffer();
            Sentence chosen = new Sentence(" ");

            while (probable.wordCount() < problem.wordCount() + 1) {
                ngramCounter++;
                ArrayList<Sentence> possibleSentences = calculateProb(probable);
                System.out.println("Original Sentence: " + problem.toString());
                System.out.println("Probable Sentence: " + probable.toString());

                int selected = 0; //always the first choice  --------------> INPUT SELECTION ALGORITHM public int selectionAlgo();

                chosen = possibleSentences.get(selected);
                probableAppend = new StringBuffer(chosen.toString());
                probableAppend.append(" " + problem.wordAt(ngramCounter - 1).toString());
                probable = new Sentence(probableAppend.toString());
            }
// show comparison result
            System.out.println("");
            problem.printComparison(original, probable);
            System.out.println("");
        }
    }

    public static ArrayList<Sentence> calculateProb(Sentence sentence) throws IOException, ClassNotFoundException {
        ArrayList<ArrayList<String>> permut = sentence.permutation();
        ArrayList<String> possibleSentences = Sentence.allCases(permut);
// sentence - log goes here..
        Map<String, Float> result = getProb(possibleSentences);
        Map<String, Float> sortedResult = sortByValue(result);

// print top 10 results
        return (printTop(10, sortedResult));
    }
//deserialize the StupidBackoffLm

    public static StupidBackoffLm commenceGoogle() throws IOException, ClassNotFoundException {
        InputStream file = null;
        try {
            file = new FileInputStream("quarks.ser");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        InputStream buffer = new BufferedInputStream(file);
        ObjectInput input = new ObjectInputStream(buffer);
        StupidBackoffLm googleModel = (StupidBackoffLm) input.readObject();
        return googleModel;
    }

    public static Map<String, Float> getProb(ArrayList<String> possibleSentence) throws IOException, ClassNotFoundException {
        System.out.println("Start Calculating probabilites... \n");
        Map<String, Float> result = new HashMap<>();
        for (String str : possibleSentence) {
            Float logProb = google.getLogProb(Arrays.asList(str.split(" ")));
            result.put(str, logProb);
        }
        return result;
    }

    public static ArrayList<Sentence> printTop(int rank, Map<String, Float> sortedLog) {
        ArrayList<Sentence> topTen = new ArrayList();
        String[] key = sortedLog.keySet().toArray(new String[sortedLog.size()]);
        if (rank > key.length) {
            rank = key.length;
        }
        System.out.println("Showing Top" + rank + " Results...");
        for (int i = 0; i < rank; i++) {
            topTen.add(new Sentence(key[i]));
            System.out.println(i + 1 + ". " + key[i] + " | Log Value: " + sortedLog.get(key[i]));
        }
        System.out.println("");
        return topTen;
    }

//compare
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });
        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

}
