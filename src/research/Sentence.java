package research;
///Without POS

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {

    StringBuffer sentence;
    int sentenceWordCount = 1;

    public Sentence(String input) {
        this.sentence = new StringBuffer(input);
        for (int i = 0; i < this.sentence.length(); i++) {
            if (sentence.charAt(i) == ' ') {
                sentenceWordCount++;
            }
        }
    }

    public void randomLetterRedaction(double probability) {
        Random rand = new Random();
        StringBuilder regex = new StringBuilder();
        regex.append("[A-Za-z]");
        Pattern regexPattern = Pattern.compile(regex.toString());
        for (int i = 0; i < sentence.length(); i++) {
            Character chr = sentence.charAt(i);
            Matcher regexMatcher = regexPattern.matcher(chr.toString());
            if (sentence.charAt(i) == ' ') {
                // do nothing
            } else {
                if (rand.nextDouble() > probability && regexMatcher.find()) {
                    sentence.replace(i, i + 1, "?");
                }
            }
        }
    }

    public int wordCount() {
        return sentenceWordCount;
    }

    public void removeLetter(Character c) {
        for (int i = 0; i < this.sentence.length(); i++) {
            if (sentence.charAt(i) == c) {
                sentence.setCharAt(i, '?');
            }
        }
    }

    public double completionPercentage() {
        double divider = getLetterCount();
        double missing = missingLetterCount();
        double percent = (divider - missing) / divider * 100;
        return percent;
    }

    public int missingLetterCount() {
        int missing = 0;
        for (int i = 0; i < this.sentence.length(); i++) {
            if (sentence.charAt(i) == '?') {
                missing++;
            }
        }
        return missing;

    }

    public double getAccuracy(Sentence guessed) {
        double size = getLetterCount();
        double missed = 0;
        for (int i = 0; i < this.sentence.length(); i++) {
            if (sentence.charAt(i) != guessed.sentence.charAt(i)) {
                missed++;
            }
        }
        double percent = (size - missed) / size * 100;
        return percent;
    }

    public int getLetterCount() {
        int size = sentence.length() - 1;
        for (int i = 0; i < size; i++) {
            if (sentence.charAt(i) == ' ') {
                size--;
            }
        }
        return size;

    }

    public String toString() {
        return this.sentence.toString();
    }

    public String wordAt(int index) {
        String sentence = this.toString();
        String[] word = sentence.split(" ");
        String ret = new String();

        for (int i = 0; i < word.length; i++) {
            if (i == index) {

                ret = word[i];

            }
        }
        return ret;
    }

    public Pattern wordToPattern(String word) {
        StringBuilder regex = new StringBuilder();
        regex.append("^");
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == '?') {
                regex.append("[A-Za-z]");
            } else {
                regex.append(word.charAt(i));
            }
        }
        //regex.append("{" + word.length() + "," + word.length() + "}");
        regex.append("$");
        Pattern regexPattern = Pattern.compile(regex.toString());
        return regexPattern;
    }

    public ArrayList<String> possibleWords(int index) throws IOException {
        StringBuilder str = new StringBuilder();
        Pattern regex = wordToPattern(wordAt(index));
        ArrayList<String> words = GoogleVocab.possibleSearch(regex);
        return words;
    }

    public void print() {
        System.out.println("Sentence: " + toString());
        System.out.println("Word Count: " + wordCount());
        System.out.println("Letter Count: " + getLetterCount());
        System.out.println("Missing Letter Count: " + missingLetterCount());
        System.out.println("Completion Percentage: " + completionPercentage() + "%");
        System.out.println("");
    }

    public Sentence copyOf() {
        String deepCopy = new String(this.sentence.toString());
        Sentence deepCopied = new Sentence(deepCopy);
        return deepCopied;
    }

    public void printComparison(Sentence original, Sentence guessed) {
        System.out.println("Problem Sentence: " + toString());
        System.out.println("Orignial Sentence: " + original.toString());
        System.out.println("Guessed Sentence: " + guessed.toString());
        System.out.println("Accuracy: " + original.getAccuracy(guessed) + "%");
        System.out.println("");
    }

    public ArrayList<ArrayList<String>> permutation() throws IOException {

        int count = 1;

        ArrayList<ArrayList<String>> permutation = new ArrayList();
        for (int i = 0; i < this.wordCount(); i++) {
            if (this.wordAt(i).contains("?")) {
                System.out.println("Generating Word at index " + i + "...");
                ArrayList<String> wordList = this.possibleWords(i);
                permutation.add(wordList);
                count = count * wordList.size();
            } else {
                ArrayList<String> setWord = new ArrayList<>();
                setWord.add(this.wordAt(i));
                permutation.add(setWord);
            }
        }
        System.out.println("# of possible sentences is: " + count);

        return permutation;
    }

    public void replaceWordAt(int index, String word) {
        String sentence = this.toString();
        String[] wordList = sentence.split(" ");
        wordList[index] = word;
        StringBuffer newSentence = new StringBuffer();
        for (int i = 0; i < wordList.length; i++) {
            if (i == 0) {
                newSentence.append(wordList[i]);
            } else {
                newSentence.append(" " + wordList[i]);
            }
        }
        this.sentence = newSentence;
    }

    public static ArrayList<String> allCases(ArrayList<ArrayList<String>> totalList) {
        System.out.println("Generating Possible Sentences...");
        ArrayList<String> result = new ArrayList<String>(totalList.get(0));

        for (int index = 1; index < totalList.size(); index++) {
            result = combineTwoLists(result, totalList.get(index));
        }
        /* print */
        //   int count = 0;
        //   for (String s : result) {
        //   System.out.printf("%d. %s\n", ++count, s);
        //   }
        return result;
    }

    public Sentence subSentence(int EndwordIndex) {
        StringBuffer subSen = new StringBuffer();
        for (int i = 0; i <= EndwordIndex; i++) {
            if (i == 0) {
                subSen.append(this.wordAt(i));
            } else {
                subSen.append(" " + this.wordAt(i));
            }
        }
        Sentence subSent = new Sentence(subSen.toString());
        return subSent;
    }

    public ArrayList<Sentence> lengthFiveSubSentence() {
        ArrayList<Sentence> array = new ArrayList<>();
        StringBuffer subSen = new StringBuffer();
        for (int i = 0; i < this.sentenceWordCount; i++) {
            if (i == 0) {
                subSen.append(this.wordAt(i));
                array.add(new Sentence(subSen.toString()));
            } else if (i < 5) {
                subSen.append(" " + this.wordAt(i));
                array.add(new Sentence(subSen.toString()));
            } else {
                StringBuffer newSubSen = new StringBuffer();
                for (int k = i - 4; k < i + 1; k++) {
                    if (k == i) {
                        newSubSen.append(this.wordAt(k));
                    } else {
                        newSubSen.append(this.wordAt(k) + " ");
                    }
                }
                array.add(new Sentence(newSubSen.toString()));
            }
        }
        return array;
    }

    private static ArrayList<String> combineTwoLists(ArrayList<String> list1, ArrayList<String> list2) {
        ArrayList<String> result = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        for (String s1 : list1) {
            for (String s2 : list2) {
                sb.setLength(0);
                sb.append(s1).append(' ').append(s2);
                result.add(sb.toString());
            }
        }
        //   System.out.println(result);
        return result;
    }

}
