
package research;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoogleVocab {
    static Set<String> googleWords = new HashSet<String>();
    
    public static void fill(){
        try {
            Scanner scan = new Scanner(new File("/Users/rk7aq/Desktop/vocab_cs"));
            while(scan.hasNext()){
                int count = 1;
                int i = 0;
                String word = scan.next();
                googleWords.add(word);
                scan.nextLine();
                if(count%100000==0){
                    i++;
                }
                count++;
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoogleVocab.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static ArrayList<String> possibleSearch(Pattern word){
        ArrayList<String> possibleWords = new ArrayList();
        CharSequence[] words;
        words = googleWords.toArray(new CharSequence[0]);
        for (int i = 0; i < words.length; i++) {
            Matcher regexMatcher = word.matcher(words[i]);
            while (regexMatcher.find()) {
                if (regexMatcher.group().length() != 0) {
                    possibleWords.add(regexMatcher.group().trim());
                }
            }
        }
        return possibleWords;
        
    }


}
